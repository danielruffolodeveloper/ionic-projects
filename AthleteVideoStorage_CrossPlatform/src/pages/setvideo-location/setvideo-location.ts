import { Component } from '@angular/core';
import { NavController,NavParams,ViewController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { Location } from '../../models/location';

@Component({
  selector: 'page-setvideo-location',
  templateUrl: 'setvideo-location.html',
})
export class SetVideoLocationPage {

  location: Location;
  marker: Location;

  constructor(private navParams: NavParams ,public navCtrl: NavController,private viewCtrl:ViewController) {
    this.location = this.navParams.get('location');
    this.marker = this.location;
  }
  
  onSetMarker(event: any) {
    console.log(event);
    this.marker = new Location(event.coords.lat,event.coords.lng);
  }
  
  onConfirm(){

        this.viewCtrl.dismiss({location: this.marker});

  }

  onabort(){
    this.viewCtrl.dismiss();

  }

}
