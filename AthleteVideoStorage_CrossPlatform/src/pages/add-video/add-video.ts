import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController,LoadingController,ToastController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { NgForm } from "@angular/forms";
import { SetVideoLocationPage } from "../setvideo-location/setvideo-location";
import { Location } from '../../models/location';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';
import { VideosService } from "../../services/videos";
import { ManageVideoPage } from "../managevideo/managevideo";




@Component({
  selector: 'page-add-video',
  templateUrl: 'add-video.html',
})



export class AddVideoPage {
    videoFilePath: string;
  

    @ViewChild('myvideo') myVideo: any;

  location: Location = {
    lat: 40.7624324,
    lng: -73.9759827
  };
  locationIsSet = false;
  vidUrl = '';
 
  constructor(public navCtrl: NavController,
  private modalCtrl: ModalController,
   private geolocation: Geolocation,
   private loadingCtrl: LoadingController,
   private toastCtrl: ToastController,
   private camera: Camera,
   private mediaCapture: MediaCapture,
   private videosService: VideosService,




   

   
   ) {}

   

   onSubmit(form: NgForm) {


    this.videosService.addVideo(form.value.title, form.value.description, this.location, this.vidUrl);
    
    form.reset();
    this.location = {
       lat: 40.7624324,
    lng: -73.9759827
  };
  
this.vidUrl = '';
this.locationIsSet = false;
this.navCtrl.push(ManageVideoPage);

  }

  onOpenMap(){
    const modal = this.modalCtrl.create(SetVideoLocationPage, {location: this.location,
    isSet: this.locationIsSet});
    modal.present();
    modal.onDidDismiss(
      data => {
        if (data) {
          this.location = data.location;
          this.locationIsSet = true;
        }
      }
    );

  }

  onLocate(){

const loader = this.loadingCtrl.create({
  content: 'pin-pointing the videos location...'
});
loader.present();

 this.geolocation.getCurrentPosition()
    .then(
      location => {
        loader.dismiss();
        this.location.lat = location.coords.latitude;
              this.location.lng = location.coords.longitude;
              this.locationIsSet = true;

          }
    )
    .catch(
      error => {
         loader.dismiss();
         const toast = this.toastCtrl.create({
  message: 'Opps, your Geolocation services are disabled',
  duration: 2500
});
toast.present();

      }
    );

  }

  onTakePhoto(){
      this.mediaCapture.captureVideo({limit:1, duration:600})

     .then((data: MediaFile[]) => {
        console.log(data);
        var i, path, len;
        for (i = 0, len = data.length; i < len; i += 1) {
          console.log(path);
      
          this.videoFilePath = data[i].fullPath;
          this.vidUrl = this.videoFilePath;
       
        }
      },
      (err: CaptureError) => {
        console.error(err);
      }
      );
      
  }



}

