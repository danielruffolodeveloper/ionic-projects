import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { EventCreatePage } from '../event-create/event-create';
import { EventListPage } from '../event-list/event-list';
import { AddVideoPage } from '../add-video/add-video';
import { Video } from "../../models/video";
import { VideosService } from "../../services/videos";
import { ManageVideoPage } from "../managevideo/managevideo";

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  






  constructor(public navCtrl: NavController,
              private videosService: VideosService) {

  }

  ionViewWillEnter(){

  }


  // method to direct nav button to Profile page activity

  goToProfile(){ this.navCtrl.push(ProfilePage); }


  // method to direct nav button to Video Create Page activity
  goToCreate(){ this.navCtrl.push(AddVideoPage); }

    // method to direct nav button to Video Create Page activity
  goToManage(){ this.navCtrl.push(ManageVideoPage); }





}
