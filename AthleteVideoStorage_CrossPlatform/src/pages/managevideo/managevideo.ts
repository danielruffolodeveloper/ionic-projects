import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { EventCreatePage } from '../event-create/event-create';
import { EventListPage } from '../event-list/event-list';
import { AddVideoPage } from '../add-video/add-video';
import { Video } from "../../models/video";
import { VideosService } from "../../services/videos";

@Component({
  selector: 'page-managevideo',
  templateUrl: 'managevideo.html',
})
export class ManageVideoPage {
  
AddVideoPage = AddVideoPage;
videos: Video[]=[]

    @ViewChild('myvideo') myVideo: any;



  constructor(public navCtrl: NavController,
              private videosService: VideosService) {

  }

  ionViewWillEnter(){

    this.videos = this.videosService.loadVideos();

  }


  // method to direct nav button to Profile page activity

  goToProfile(){ this.navCtrl.push(ProfilePage); }


  // method to direct nav button to Video Create Page activity
  goToCreate(){ this.navCtrl.push(AddVideoPage); }




}
