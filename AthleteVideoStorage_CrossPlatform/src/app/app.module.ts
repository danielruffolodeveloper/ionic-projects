import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';

// Import pages
import { ManageVideoPage } from '../pages/managevideo/managevideo';
import { EventCreatePage } from '../pages/event-create/event-create';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { EventListPage } from '../pages/event-list/event-list';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { SignupPage } from '../pages/signup/signup';
//_____________________________________________________________//
// Added 3 pages for video capture
import { AddVideoPage } from '../pages/add-video/add-video';
import { SetVideoLocationPage } from '../pages/setvideo-location/setvideo-location';
import { VideoPage } from '../pages/video/video';
import { DashboardPage } from '../pages/dashboard/dashboard';
//_____________________________________________________________//
// Import providers
import { GetProviders } from './app.providers';
import { AgmCoreModule } from "angular2-google-maps/core";
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';
import { VideosService } from "../services/videos";

@NgModule({
  declarations: [
    MyApp,
    ManageVideoPage,
    EventCreatePage,
    EventDetailPage,
    EventListPage,
    LoginPage,
    ProfilePage,
    DashboardPage,
    ResetPasswordPage,
    SignupPage,
    AddVideoPage,
    SetVideoLocationPage,
    VideoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCRYKInaeE8WIdBFxw-7qKTqN-QrUbe08w'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ManageVideoPage,
    EventCreatePage,
    EventDetailPage,
    EventListPage,
    LoginPage,
    ProfilePage,
    ResetPasswordPage,
    SignupPage,
    DashboardPage,
    AddVideoPage,
    SetVideoLocationPage,
    VideoPage

  ],
  providers:[
  Geolocation,
    Camera,
    MediaCapture,
    VideosService,
   GetProviders()
  ]
    

  
})
export class AppModule {}

