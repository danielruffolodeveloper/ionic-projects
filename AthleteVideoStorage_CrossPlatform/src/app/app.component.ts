import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ManageVideoPage } from '../pages/managevideo/managevideo';
import { LoginPage } from '../pages/login/login';

import firebase from 'firebase';
import { DashboardPage } from "../pages/dashboard/dashboard";


@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage: any;
  zone: NgZone;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.zone = new NgZone({});
    firebase.initializeApp({

      apiKey: "AIzaSyC7H6Xb_EvzQTgINsU10Ta9H3TtI1LREX8",
    authDomain: "avsdeakin.firebaseapp.com",
    databaseURL: "https://avsdeakin.firebaseio.com",
    projectId: "avsdeakin",
    storageBucket: "avsdeakin.appspot.com",
    messagingSenderId: "289080362429"

    });

    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      this.zone.run( () => {
        if (!user) {
          this.rootPage = LoginPage;
          unsubscribe();
        } else { 
          this.rootPage = DashboardPage;
          unsubscribe();
        }
      });     
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}