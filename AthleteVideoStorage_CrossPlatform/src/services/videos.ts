import { Video } from "../models/video";
import { Location } from "../models/location";

export class VideosService {

    private videos: Video[] = [];

    addVideo(title: string,
             description: string, 
            location: Location,
            vidUrl: string)
            
            {
                 const video = new Video(title, description, location, vidUrl);
                 this.videos.push(video);
             }

             loadVideos(){
                 return this.videos.slice();
             }
}

