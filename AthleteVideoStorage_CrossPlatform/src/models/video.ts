
import {Location} from "./location";
export class Video {
    constructor(public title: string,
                public description: string,
                public location: Location,
                public vidUrl: string){
                    
                }
}